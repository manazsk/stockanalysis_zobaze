
# coding: utf-8

# In[1]:


import pymongo
import pandas as pd
from flask import Flask, redirect, render_template, request, make_response
import time
from crontab import CronTab

app = Flask(__name__)


# In[2]:


def startjob(command):
    
    cron = CronTab(user ='manaz')  
    job = cron.new(command = command) 
    job.minute.every(1)

    cron.write()
    
    return


# In[3]:


@app.route('/download')
def stopjob():

    cron = CronTab(user = 'manaz')  

    cron.remove_all()

    cron.write()
    
    myclient = pymongo.MongoClient('mongodb://admin:zobaze123@localhost:27017/')
    mydb = myclient["stockdata"]
    mycol = mydb[sym]
    
    cursor = mycol.find({},{"_id":0,"type":1,"entry":1,"exit":1,"Buy_Time":1,"Sell_Time":1,"P/L":1})
    df = pd.DataFrame(list(cursor))
    net = int(df['P/L'].sum())
    
    resp = make_response(df.to_csv())
    resp.headers["Content-Disposition"] = "attachment; filename=summary.csv"
    resp.headers["Content-Type"] = "text/csv"
    
    return resp


# In[4]:


@app.route('/')
def form():
    return render_template('front2.html')


# In[5]:


@app.route('/submit', methods= ["POST"])
def parseform():
    
    result = request.form
    e =[]
    x = []
    global sym
    
    sym = result['stocks']
    candle = result['candle']
    bsize = result['bsize']
    rule = '2'
    
    entry = ""
    exit = ""
    
    e.append(result['l1'])
    e.append(result['l2'])
    e.append(result['s1'])
    e.append(result['s2'])
    
    x.append(result['l3'])
    x.append(result['l4'])
    x.append(result['s3'])
    x.append(result['s4'])
    
    entry = entry.join(e)
    exit = exit.join(x)
    
    
    command = "cd /home/ubuntu/final/ && python "+"final_cron.py "+sym+" "+candle+" "+bsize+" "+rule+" "+entry+" "+exit
    
    startjob(command)
    
    time.sleep(50)
    
    return render_template('front3.html')


# In[6]:


if __name__ == "__main__":
    
    app.run(host = '0.0.0.0', port = 1235, debug = True)

