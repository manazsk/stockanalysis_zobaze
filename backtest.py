
# coding: utf-8

# In[ ]:


import pandas as pd
import json
import requests
from datetime import datetime
#import talib as ta
import numpy as np
from statistics import mean


# In[ ]:


def parse(data,bsize,rule):
    
    count = len(data['sub'])
    
    prev_close = int(data['main']['open'])
    
    prev_close = round(prev_close)
    
    iter_list = []
    time_list = []
    open_list = []
    close = []
    time = []
    
    for i in range(1,count+1):
        i = str(i)
        iter_list.append(data['sub'][i]['p'])
        iter_list.append(data['sub'][i]['n'])
        time_list.append(data['sub'][i]['timestamp'])
        
    k = -1
    t = -1
    #print(time_list)
    
    for i in iter_list:
        
        if(i!=0):
            t = t+1
        q = int(i/bsize)
        rem = abs(i)%bsize
        if(rem > (bsize/2) and rule=='2'):
            c = abs(q)+1
        else:
            c = abs(q)
        for j in range(c):
            k = k + 1
            open_list.append(prev_close)
            time.append(time_list[t])
            if(i>0):
                close.append(open_list[k] + bsize)
            else:
                close.append(open_list[k] - bsize)
            prev_close = close[k]
            
            
    df = pd.DataFrame()
              
    df['open'] = open_list
    df['close']= close
    df['time']= time
    
    df['time'] = pd.to_datetime(df['time'])
    df['time'] = df['time'].apply( lambda d : d.time() )
     
    
    return df


# In[ ]:


def BUY(df,index,sema,entry,exit,category,time_b,time_s):
    
    if(sema == 0):
        entry.append(df['open'].loc[index])
        category.append("LONG")
        time_b.append(df['time'].loc[index])
        sema = 1
    else:
        exit.append(df['open'].loc[index])
        time_b.append(df['time'].loc[index])
        sema = 0
    
    #print(index)
    return index,sema,entry,exit,category,time_b,time_s


def SELL(df,index,sema,entry,exit,category,time_b,time_s):
    
    if(sema == 0):
        entry.append(df['open'].loc[index])
        category.append("SHORT")
        time_s.append(df['time'].loc[index])
        sema = 1
    else:
        exit.append(df['open'].loc[index])
        time_s.append(df['time'].loc[index])
        sema = 0
        
    #print(index)
    return index,sema,entry,exit,category,time_b,time_s


# In[ ]:


def yesstate(df,e,x,color,state,index,entry,exit,sema,category,time_b,time_s):
    
    color1 = color[index:]
    
    lists = ['green']*x[0] + ['red']*x[1]
    listb = ['green']*x[2] + ['red']*x[3]
    
    g = x[2]+x[3]
    r = x[0]+x[1]
    
    j = 0
    
    if(g>r):
        d = g
    else:
        d = r
    
    while((j+d)<=len(color1)):
        
        list_temp = []
        list_temp2 = []
    
        for i in range(j,j+g):
        
            list_temp.append(color1[i])
            
        for k in range(j,j+r):
            
            list_temp2.append(color1[k])
            
        j = j+1
        
        if(list_temp == listb and state == 'YESBUY'):
            index1 = index + i + 1
            if(index1<len(color)):
                index = index1
                #print("buy@"+str(index))
                index,sema,entry,exit,category,time_b,time_s = BUY(df,index,sema,entry,exit,category,time_b,time_s)
                state = 'NO'
                break
        
        elif(list_temp2 == lists and state == 'YESSELL'):
            index1 = index + k + 1
            if(index1<len(color)):
                index = index1
                #print("sell@"+str(index))
                index,sema,entry,exit,category,time_b,time_s = SELL(df,index,sema,entry,exit,category,time_b,time_s)
                state = 'NO'
                break
            
        else:
            continue
            
    return index,state,entry,exit,sema,category,time_b,time_s


# In[ ]:


def nostate(df,e,x,color,state,index,entry,exit,sema,category,time_b,time_s):
    
    color1 = color[index:]
    
    listb = ['green']*e[0] + ['red']*e[1]
    lists = ['green']*e[2] + ['red']*e[3]
    
    g = e[0]+e[1]
    r = e[2]+e[3]
    
    j = 0
    
    if(g>r):
        d = g
    else:
        d = r
    
    while((j+d)<=len(color1)):
        
        list_temp = []
        list_temp2 = []
    
        for i in range(j,j+g):
        
            list_temp.append(color1[i])
            
        for k in range(j,j+r):
            
            list_temp2.append(color1[k])
            
        j = j+1
        
        if(list_temp == listb and state=='NO'):
            index1 = index + i + 1
            if(index1<len(color)):
                index = index1
                index,sema,entry,exit,category,time_b,time_s = BUY(df,index,sema,entry,exit,category,time_b,time_s)
                state = 'YESSELL'
                #print("buy@"+str(index))
                break
        
        elif(list_temp2 == lists and state=='NO'):
            index1 = index + k + 1
            if(index1<len(color)):
                index = index1
                index,sema,entry,exit,category,time_b,time_s = SELL(df,index,sema,entry,exit,category,time_b,time_s)
                state = 'YESBUY'
                #print("sell@"+str(index))
                break
            
        else:
            continue
    
    #print(index)
            
    return index,state,entry,exit,sema,category,time_b,time_s


# In[ ]:


def check_state(df,e,x,state):
    
    index = 0
    entry = []
    exit = []
    time_b = []
    time_s = []
    category = []
    sema = 0
    
    df['color'] = df['color']
    
    color = df['color'].tolist()
    print(df)
    count = len(color)
    i = 0
    while (i < count):
        
        i=i+1
        
        index,state,entry,exit,sema,category,time_b,time_s = nostate(df,e,x,color,state,index,entry,exit,sema,category,time_b,time_s)
    
        index,state,entry,exit,sema,category,time_b,time_s = yesstate(df,e,x,color,state,index,entry,exit,sema,category,time_b,time_s)
    
    #print(state)
    
    df2 = pd.DataFrame()
    df3 = pd.DataFrame()
    df4 = pd.DataFrame()
    df5 = pd.DataFrame()
    
    df2['type'] = category
    df2['entry']  = entry
    df3['exit'] = exit
    
    df4['Buy_Time'] = time_b
    df5['Sell_Time'] = time_s
    
    long_buy = []
    short_sell = []
    
    for i in range(len(entry)):
        if(category[i]=="LONG"):
            long_buy.append(entry[i])
        else:
            short_sell.append(entry[i])
            
    if(len(long_buy)!=0):
        buy_avg = mean(long_buy)
    else:
        buy_avg = 'NA'
    
    if(len(short_sell)!=0):
        sell_avg = mean(short_sell)
    else:
        sell_avg = 'NA'
    
    result = pd.concat([df2,df3,df4,df5],axis = 1)
    
    result['P/L'] = result['entry'] - result['exit']
    
    return result,buy_avg,sell_avg


# In[ ]:


def lambda_handler(event, context):
    
    body = {}
    
    for key, value in event.items() :
        body[key] = value
    
    url = body['body']
    params = url.split('&')
    temp = []
    for x in params:
        temp.append(x.split('='))
        
    temp = temp[:-1]
    
    form = dict(temp)
    
    start = form['sdate']
    end = form['edate']
    candle = form['candle']
    bsize = float(form['bsize'])
    rule = form['rule']

    e = []
    x = []

    e.append(form['b1'])
    e.append(form['b2'])
    e.append(form['b3'])
    e.append(form['b4'])

    x.append(form['b5'])
    x.append(form['b6'])
    x.append(form['b7'])
    x.append(form['b8'])

    e = [ int(y) for y in e ]
    x = [ int(y) for y in x ]

    
    start = datetime.strptime(start, "%Y-%m-%d").strftime("%Y%m%d")
    end = datetime.strptime(end, "%Y-%m-%d").strftime("%Y%m%d")
    
    custom = str(0)
    URL = "http://13.232.193.224:1234/"+candle+"/"+start+"/"+end+"/"+custom
    s = requests.get(url = URL)
    data = s.json()
    
    df = parse(data,bsize,rule)
    
    state = 'NO'
    
    df['color'] = np.where(df['close']>df['open'],'green','red')
    
    result,buy_avg,sell_avg = check_state(df,e,x,state)
    
    p_l = result['P/L'].tolist()
    
    total_p = 0
    total_l = 0
    
    for x in p_l:
        if(x>0):
            total_p += x
        elif(x<0):
            total_l += x
        else:
            continue
    
    net = result['P/L'].sum()
    
    if(buy_avg!='NA'):
        long_percent = round((net/buy_avg)*100,2)
    else:
        long_percent = 'NA'
        
    if(sell_avg!='NA'):
        short_percent = round((net/sell_avg)*100,2)
    else:
        short_percent = 'NA'
        
    result1 = {}
    result1['total_profit'] = str(total_p)
    result1['total_loss'] = str(total_l)
    result1['net'] = str(net)
    result1['long_percent'] = str(long_percent)
    result1['short_percent'] = str(short_percent)
    
    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},
        'body': json.dumps(result1)
    }

