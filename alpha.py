
# coding: utf-8

# In[1]:


import pymongo
from alpha_vantage.timeseries import TimeSeries
import pandas as pd
from datetime import datetime
from statistics import mean
import json
import numpy as np


# In[2]:


def parse(data,bsize,rule):
    
    count = len(data['sub'])
    
    prev_close = int(data['main']['open'])
    
    prev_close = round(prev_close)
    
    iter_list = []
    time_list = []
    open_list = []
    close = []
    time = []
    
    for i in range(1,count+1):
        iter_list.append(data['sub'][i]['p'])
        iter_list.append(data['sub'][i]['n'])
        time_list.append(data['sub'][i]['timestamp'])
        
    k = -1
    t = -1
    #print(time_list)
    
    for i in iter_list:
        
        if(i!=0):
            t = t+1
        q = int(i/bsize)
        rem = abs(i)%bsize
        if(rem > (bsize/2) and rule=='2'):
            c = abs(q)+1
        else:
            c = abs(q)
        for j in range(c):
            k = k + 1
            open_list.append(prev_close)
            time.append(time_list[t])
            if(i>0):
                close.append(open_list[k] + bsize)
            else:
                close.append(open_list[k] - bsize)
            prev_close = close[k]
            
            
    df = pd.DataFrame()
              
    df['open'] = open_list
    df['close']= close
    df['time']= time
    
    #print(df)
    
    #df['time'] = pd.to_datetime(df['time'])
     
    return df


# In[3]:


def BUY(df,index,sema,entry,exit,category,time_b,time_s):
    
    if(sema == 0):
        entry.append(df['open'].loc[index])
        category.append("LONG")
        time_b.append(df['time'].loc[index])
        sema = 1
    else:
        exit.append(df['open'].loc[index])
        time_b.append(df['time'].loc[index])
        sema = 0
    
    #print(index)
    return index,sema,entry,exit,category,time_b,time_s


# In[4]:


def SELL(df,index,sema,entry,exit,category,time_b,time_s):
    
    if(sema == 0):
        entry.append(df['open'].loc[index])
        category.append("SHORT")
        time_s.append(df['time'].loc[index])
        sema = 1
    else:
        exit.append(df['open'].loc[index])
        time_s.append(df['time'].loc[index])
        sema = 0
        
    #print(index)
    return index,sema,entry,exit,category,time_b,time_s


# In[5]:


def yesstate(df,e,x,color,state,index,entry,exit,sema,category,time_b,time_s):
    
    color1 = color[index:]
    
    lists = ['green']*x[0] + ['red']*x[1]
    listb = ['green']*x[2] + ['red']*x[3]
    
    g = x[2]+x[3]
    r = x[0]+x[1]
    
    j = 0
    
    if(g>r):
        d = g
    else:
        d = r
    
    while((j+d)<=len(color1)):
        
        list_temp = []
        list_temp2 = []
    
        for i in range(j,j+g):
        
            list_temp.append(color1[i])
            
        for k in range(j,j+r):
            
            list_temp2.append(color1[k])
            
        j = j+1
        
        if(list_temp == listb and state == 'YESBUY'):
            index1 = index + i + 1
            if(index1<len(color)):
                index = index1
                #print("buy@"+str(index))
                index,sema,entry,exit,category,time_b,time_s = BUY(df,index,sema,entry,exit,category,time_b,time_s)
                state = 'NO'
                break
        
        elif(list_temp2 == lists and state == 'YESSELL'):
            index1 = index + k + 1
            if(index1<len(color)):
                index = index1
                #print("sell@"+str(index))
                index,sema,entry,exit,category,time_b,time_s = SELL(df,index,sema,entry,exit,category,time_b,time_s)
                state = 'NO'
                break
            
        else:
            continue
            
    return index,state,entry,exit,sema,category,time_b,time_s


# In[6]:


def nostate(df,e,x,color,state,index,entry,exit,sema,category,time_b,time_s):
    
    color1 = color[index:]
    
    listb = ['green']*e[0] + ['red']*e[1]
    lists = ['green']*e[2] + ['red']*e[3]
    
    g = e[0]+e[1]
    r = e[2]+e[3]
    
    j = 0
    
    if(g>r):
        d = g
    else:
        d = r
    
    while((j+d)<=len(color1)):
        
        list_temp = []
        list_temp2 = []
    
        for i in range(j,j+g):
        
            list_temp.append(color1[i])
            
        for k in range(j,j+r):
            
            list_temp2.append(color1[k])
            
        j = j+1
        
        if(list_temp == listb and state=='NO'):
            index1 = index + i + 1
            if(index1<len(color)):
                index = index1
                index,sema,entry,exit,category,time_b,time_s = BUY(df,index,sema,entry,exit,category,time_b,time_s)
                state = 'YESSELL'
                #print("buy@"+str(index))
                break
        
        elif(list_temp2 == lists and state=='NO'):
            index1 = index + k + 1
            if(index1<len(color)):
                index = index1
                index,sema,entry,exit,category,time_b,time_s = SELL(df,index,sema,entry,exit,category,time_b,time_s)
                state = 'YESBUY'
                #print("sell@"+str(index))
                break
            
        else:
            continue
    
    #print(index)
            
    return index,state,entry,exit,sema,category,time_b,time_s


# In[7]:


def check_state(df,e,x,state):
    
    index = 0
    entry = []
    exit = []
    time_b = []
    time_s = []
    category = []
    sema = 0
    
    color = df['color'].tolist()
    #print(df)
    count = len(color)
    i = 0
    while (i < count):
        
        i=i+1
        
        index,state,entry,exit,sema,category,time_b,time_s = nostate(df,e,x,color,state,index,entry,exit,sema,category,time_b,time_s)
    
        index,state,entry,exit,sema,category,time_b,time_s = yesstate(df,e,x,color,state,index,entry,exit,sema,category,time_b,time_s)
    
    #print(state)
    
    df2 = pd.DataFrame()
    df3 = pd.DataFrame()
    df4 = pd.DataFrame()
    df5 = pd.DataFrame()
    
    df2['type'] = category
    df2['entry']  = entry
    df3['exit'] = exit
    
    df4['Buy_Time'] = time_b
    df5['Sell_Time'] = time_s
    
    long_buy = []
    short_sell = []
    
    for i in range(len(entry)):
        if(category[i]=="LONG"):
            long_buy.append(entry[i])
        else:
            short_sell.append(entry[i])
            
    if(len(long_buy)!=0):
        buy_avg = mean(long_buy)
    else:
        buy_avg = 'NA'
    
    if(len(short_sell)!=0):
        sell_avg = mean(short_sell)
    else:
        sell_avg = 'NA'
    
    result = pd.concat([df2,df3,df4,df5],axis = 1)
    
    result['P/L'] = result['entry'] - result['exit']
    
    return result,buy_avg,sell_avg


# In[8]:


def relative(df):
    
    close_list = df['close'].tolist()
    date_list = df['date'].tolist()

    rel_dict = {}
    rel_dict['main'] = {}
    rel_dict['sub'] = {}
    
    for i in range(1,len(close_list)):
        x = close_list[i] - close_list[i-1]
        x = round(x,2)
        rel_dict['sub'][i] = {}
        if(x>0):
            rel_dict['sub'][i]['p'] = x
            rel_dict['sub'][i]['n'] = 0
            rel_dict['sub'][i]['c'] = abs(x)
        elif(x<0):
            rel_dict['sub'][i]['n'] = x
            rel_dict['sub'][i]['p'] = 0
            rel_dict['sub'][i]['c'] = abs(x)
        else:
            rel_dict['sub'][i]['p'] = rel_dict['sub'][i]['n'] = rel_dict['sub'][i]['c'] = 0
            
        rel_dict['sub'][i]['timestamp'] = str(date_list[i])
        
    list_p = []
    list_n = []
    list_c = []
    
    for i in range(1,len(close_list)):
        list_p.append(rel_dict['sub'][i]['p'])
        list_n.append(rel_dict['sub'][i]['n'])
        list_c.append(rel_dict['sub'][i]['c'])
        
    rel_dict['main']['open'] = close_list[0]
    
    return rel_dict


# In[9]:


def preprocess(sym,candle):
    
    t = datetime.now()
    
    keys = ['87K9400LVV7E7FM3','FFHW7HUS0LPCINH2','ZTXPKZTIPB8NDXK2','ZNR5FCMD9GJM9FQ1']
    
    if(t.hour < 6):
        api_key = keys[0]
    elif(t.hour >= 6 and t.hour < 12):
        api_key = keys[1]
    elif(t.hour >= 12 and t.hour < 18):
        api_key = keys[2]
    else:
        api_key = keys[3]
        
    ts = TimeSeries(key=api_key,retries = 8,output_format='pandas')
    data, meta_data = ts.get_intraday(symbol = sym,interval = candle, outputsize='compact')
    
    data = pd.DataFrame(data)
    data.columns = ['open','high','low','close','volume']
    
    data = data.drop("volume", axis=1)
    data = data.round({'open':2,'high':2,'low':2,'close':2})
    
    data.reset_index(inplace=True)
    
    #data['date'] = pd.to_datetime(data['date'])
    return data


# In[10]:


def output(result,sym):

    data1 = json.loads(result.to_json( orient='records'))
    #data1['Buy_Time'] = data1['Buy_Time'].astype(str)
    #data1['Sell_Time'] = data1['Sell_Time'].astype(str)
    
    
    myclient = pymongo.MongoClient('mongodb://admin:zobaze123@13.232.193.224:27017/')
    mydb = myclient["stockdata"]
    mycol = mydb[sym]
    
    a = mycol.distinct('Buy_Time')
    b = mycol.distinct('Sell_Time')
    
    if(mycol.count() == 0):
        mycol.insert_many(data1)
    
    else:
        for x in data1:
            if(x.get('Buy_Time') in a and x.get('Sell_Time') in b):
                continue
            else:
                mycol.insert_one(x)
    
    cursor = mycol.find({},{"_id":0,"type":1,"entry":1,"exit":1,"Buy_Time":1,"Sell_Time":1,"P/L":1})
    df = pd.DataFrame(list(cursor))
    #print(df)
    
    return


# In[11]:


def lambda_handler(event, context):
    
    sym = event['queryStringParameters']['sym']
    candle = event['queryStringParameters']['candle']
    bsize = float(event['queryStringParameters']['b'])
    rule = event['queryStringParameters']['rule']
    entry = event['queryStringParameters']['entry']
    exit = event['queryStringParameters']['exit']
    
    e = []
    x = []
    
    for i,j in zip(entry,exit):
        e.append(int(i))
        x.append(int(j))
    
    data = preprocess(sym,candle)
    data = relative(data)
    df = parse(data,bsize,rule)
    
    state = 'NO'
    
    df['color'] = np.where(df['close']>df['open'],'green','red')
    
    result,buy_avg,sell_avg = check_state(df,e,x,state)
    
    result.fillna("NA")
    #print(result)
    
    output(result,sym)
    
    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},
        'body': json.dumps("success")
    }

