#!/usr/bin/env python
# coding: utf-8

# In[1]:


import requests
import time
import pymongo
import json
import statistics
import logging
import sys
import numpy as np
from sshtunnel import SSHTunnelForwarder
from stockstats import StockDataFrame as Sdf
from alpha_vantage.timeseries import TimeSeries
from pandas_datareader import data
import matplotlib.pyplot as plt
import pandas as pd
import sendgrid
import os
from sendgrid.helpers.mail import *
import datetime


# In[2]:


def email(txt):
    
    sg = sendgrid.SendGridAPIClient(apikey='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')
    from_email = Email("sdkumardutta@gmail.com")
    to_email = Email("k@zobaze.com")
    subject = "[IMPORTANT]- Stock Market"
    content = Content("text/plain",txt)
    mail = Mail(from_email, subject, to_email, content)
    response = sg.client.mail.send.post(request_body=mail.get())
    print(response.status_code)
    print(response.body)
    print(response.headers)


# In[3]:


def sma(df,signals):
    short_window = 10
    long_window = 20
    
    #short sma
    signals['short_sma'] = df['close'].rolling(window = short_window, min_periods=1, center= False).mean()

    #long_sma
    signals['long_sma']  = df['close'].rolling(window = long_window, min_periods=1, center = False).mean()
    
    return signals


# In[4]:


def rsi(df,signals):
    df1 = df

    stock_df = Sdf.retype(df1)
    signals['rsi'] = stock_df['rsi_14']

    del df['close_-1_s']
    del df['close_-1_d']
    del df['rs_14']
    del df['rsi_14']
    
    return signals


# In[5]:


def insertion():
    
    t = datetime.datetime.now()
    
    keys = ['87K9400LVV7E7FM3','FFHW7HUS0LPCINH2','ZTXPKZTIPB8NDXK2']
    
    if(t.hour < 8):
        api_key = keys[0]
    elif(t.hour >= 8 and t.hour < 16):
        api_key = keys[1]
    else:
        api_key = keys[2]
        
    time.sleep(30)
    ts = TimeSeries(key=api_key,output_format='pandas')
    data, meta_data = ts.get_intraday(symbol='BTCUSD',interval = '1min', outputsize='compact')
    data = pd.DataFrame(data)
    
    data.columns = ['open','high','low','close','volume']
    
    data.reset_index(inplace=True)
    
    data = data.round({'open':2,'high':2,'low':2,'close':2})
    
    data1 = json.loads(data.to_json( orient='records'))
    
    a = mycol.distinct('date') 
    if(mycol.count() == 0):
        mycol.insert_many(data1)
    
    else:
        for x in data1:
            if(x.get('date') in a):
                continue
            else:
                mycol.insert_one(x)


    cursor = mycol.find({},{"_id":0,"date":1,"close":1})

    df = pd.DataFrame(list(cursor))

    df.set_index('date',inplace=True)

    #print(df.tail(3))
    
    signals = pd.DataFrame(index = df.index)
    signals['close'] = df['close']
    
    signals = rsi(df,signals)
    signals = sma(df,signals)
    
    return signals


# In[6]:


def buy(close_val):
    
    txt = "BUY@"
    txt+= str(close_val)
    #print(txt)
    
    global checklist
    
    if(txt in checklist):
        return
    else:
        checklist.append(txt)
        email(txt)


# In[7]:


def sell(close_val):
    
    txt = "SELL@"
    txt+= str(close_val)
    #print(txt)
    
    global checklist
    
    if(txt in checklist):
        return
    else:
        checklist.append(txt)
        email(txt)


# In[8]:


def long(index,signals,close_val):
    
    buy(close_val)
    out = 0
    while True:
        if(out == 1):
            break
        else:
            signals = insertion()
            for i,r in signals.loc[index:].iterrows():     #problematic area
                if(r['rsi'] < 70):
                    sell(r['close'])
                    out = 1
                    break
                else:
                    continue


# In[9]:


def short(index,signals,close_val):
    
    sell(close_val)
    out = 0
    while True:
        if(out == 1):
            break
        else:
            signals = insertion()
            for i,r in signals.loc[index:].iterrows():
                if(r['rsi'] > 30):
                    buy(r['close'])
                    out = 1
                    break
                else:
                    continue


# In[10]:


def triggers(signals):
    
    for index,rows in signals.iterrows():
        if(rows['short_sma'] > rows['long_sma'] and rows['rsi'] > 70):
            long(index,signals,rows['close'])
            
        elif(rows['short_sma'] < rows['long_sma'] and rows['rsi'] < 30):
            short(index,signals,rows['close'])
            
        else:
            continue


# In[11]:


logging.basicConfig(filename = "app.log",filemode = "a")
log = open("app.log","a")
sys.stdout = log


# In[12]:


#MONGO_HOST = "159.65.133.177"
#MONGO_USER = "root"
#MONGO_PASS = "asus@0000"

#server = SSHTunnelForwarder(
    #MONGO_HOST,
    #ssh_username=MONGO_USER,
    #ssh_password=MONGO_PASS,
    #remote_bind_address=('127.0.0.1', 27017)
#)

#server.start()

#myclient = pymongo.MongoClient('127.0.0.1', server.local_bind_port)
myclient = pymongo.MongoClient('localhost',27017)

mydb = myclient["mydatabase1"]
mycol = mydb["stockdata"]


# In[13]:


checklist = []

while True:
    signals = insertion()
    #print(signals)
    triggers(signals)
    #print(signals)
    print(checklist)

