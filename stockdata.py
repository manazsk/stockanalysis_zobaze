#!/usr/bin/env python
# coding: utf-8

# In[4]:


import pymongo
import json
import numpy as np
from alpha_vantage.timeseries import TimeSeries
from pandas_datareader import data
import pandas as pd
import datetime


# In[5]:


def insertion(mydb):

    onecol = mydb["onemin"]
    fivecol = mydb["fivemin"]
    fifteencol = mydb["fifteenmin"]
    thirtycol = mydb["thirtymin"]
    hourcol = mydb["hour"]
    
    t = datetime.datetime.now()
    
    keys = ['87K9400LVV7E7FM3','FFHW7HUS0LPCINH2','ZTXPKZTIPB8NDXK2','ZNR5FCMD9GJM9FQ1']
    
    if(t.hour < 6):
        api_key = keys[0]
    elif(t.hour >= 6 and t.hour < 12):
        api_key = keys[1]
    elif(t.hour >= 12 and t.hour < 18):
        api_key = keys[2]
    else:
        api_key = keys[3]
        
    #time.sleep(60)
    ts = TimeSeries(key=api_key,retries = 8,output_format='pandas')
    data, meta_data = ts.get_intraday(symbol='AMZN',interval = '1min', outputsize='full')
    
    data = pd.DataFrame(data)
    data.columns = ['open','high','low','close','volume']
    
    
    data.reset_index(inplace=True)
    
    data = data.round({'open':2,'high':2,'low':2,'close':2})
    
    data1 = json.loads(data.to_json( orient='records'))
    
    a = onecol.distinct('date') 
    if(onecol.count() == 0):
        onecol.insert_many(data1)
    
    else:
        for x in data1:
            if(x.get('date') in a):
                continue
            else:
                onecol.insert_one(x)


    cursor = onecol.find({},{"_id":0,"date":1,"open":1,"high":1,"low":1,"close":1,"volume":1})
    df = pd.DataFrame(list(cursor))
    df['date'] = pd.to_datetime(df['date'])
    df.set_index("date", inplace = True)
    
    #print(df.tail(3))
    
    ohlc_dict = {
    'open':'first',
    'high':'max',
    'low':'min',
    'close':'last',
    'volume':'sum'
    }
    
    df_5min = df.resample('5Min').agg(ohlc_dict).dropna()
    df_15min = df.resample('15Min').agg(ohlc_dict).dropna()
    df_30min = df.resample('30Min').agg(ohlc_dict).dropna()
    df_1hr = df.resample('H').agg(ohlc_dict).dropna()

    #print(df_30min.tail())

    df_5min.reset_index(inplace=True)
    df_15min.reset_index(inplace=True)
    df_30min.reset_index(inplace=True)
    df_1hr.reset_index(inplace=True)
    
    
    data2 = df_5min.to_dict( orient='records')
    
    
    a = fivecol.distinct('date') 
    if(fivecol.count() == 0):
        fivecol.insert_many(data2)
    
    else:
        for x in data2:
            if(x.get('date') in a):
                continue
            else:
                fivecol.insert_one(x)
    
    data3 = df_15min.to_dict( orient='records')
    
    a = fifteencol.distinct('date') 
    if(fifteencol.count() == 0):
        fifteencol.insert_many(data3)
    
    else:
        for x in data3:
            if(x.get('date') in a):
                continue
            else:
                fifteencol.insert_one(x)
    
    
    data4 = df_30min.to_dict( orient='records')
    
    a = thirtycol.distinct('date') 
    if(thirtycol.count() == 0):
        thirtycol.insert_many(data4)
    
    else:
        for x in data4:
            if(x.get('date') in a):
                continue
            else:
                thirtycol.insert_one(x)
    
    data5 = df_1hr.to_dict( orient='records')
    
    a = hourcol.distinct('date') 
    if(hourcol.count() == 0):
        hourcol.insert_many(data5)
    
    else:
        for x in data5:
            if(x.get('date') in a):
                continue
            else:
                hourcol.insert_one(x)

    #print("success")

    return
    


# In[6]:


def lambda_handler(event, context):
    
    myclient = pymongo.MongoClient('mongodb://admin:zobaze123@13.232.193.224:27017/')

    mydb = myclient["stockdata"]

    insertion(mydb)

    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }


# In[ ]:




