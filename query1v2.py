
# coding: utf-8

# In[1]:


import pymongo
from datetime import datetime
import json
import pandas as pd
import time
from statistics import mean


# In[2]:


def mask(mycol,start,end):
    start = datetime.strptime(str(start), '%Y%m%d').strftime('%Y-%m-%d')
    end  =  datetime.strptime(str(end), '%Y%m%d').strftime('%Y-%m-%d')
    
    start = pd.to_datetime(start).date()
    end = pd.to_datetime(end).date()
    
    cursor = mycol.find({},{"_id":0,"date":1,"close":1})
    df = pd.DataFrame(list(cursor))
    df['date'] = pd.to_datetime(df['date'])
    df = df[["date","close"]]
    
    df1 = pd.DataFrame()
    df1['date'] = df['date'].dt.date
    
    
    mask = (df1['date'] >= start) & (df1['date'] <= end)
    
    df = df.loc[mask]
    
    return df


# In[3]:


def relative(df):
    
    close_list = df['close'].tolist()
    date_list = df['date'].tolist()

    rel_dict = {}
    rel_dict['main'] = {}
    rel_dict['sub'] = {}
    
    for i in range(1,len(close_list)):
        x = close_list[i] - close_list[i-1]
        x = round(x,2)
        rel_dict['sub'][i] = {}
        if(x>0):
            rel_dict['sub'][i]['p'] = x
            rel_dict['sub'][i]['n'] = 0
            rel_dict['sub'][i]['c'] = abs(x)
        elif(x<0):
            rel_dict['sub'][i]['n'] = x
            rel_dict['sub'][i]['p'] = 0
            rel_dict['sub'][i]['c'] = abs(x)
        else:
            rel_dict['sub'][i]['p'] = rel_dict['sub'][i]['n'] = rel_dict['sub'][i]['c'] = 0
            
        rel_dict['sub'][i]['timestamp'] = str(date_list[i])
        
    list_p = []
    list_n = []
    list_c = []
    
    for i in range(1,len(close_list)):
        list_p.append(rel_dict['sub'][i]['p'])
        list_n.append(rel_dict['sub'][i]['n'])
        list_c.append(rel_dict['sub'][i]['c'])
        
    rel_dict['main']['avg_p'] = round(mean(list_p),2)
    rel_dict['main']['avg_n'] = round(mean(list_n),2)
    rel_dict['main']['avg_c'] = round(mean(list_c),2)
    rel_dict['main']['open'] = close_list[0]
    rel_dict['main']['close'] = close_list[i]
    
    return rel_dict


# In[4]:


def lambda_handler(event, context):

    candle = event['queryStringParameters']['candle']
    start = event['queryStringParameters']['start']
    end = event['queryStringParameters']['end']
    
    myclient = pymongo.MongoClient('mongodb://admin:zobaze123@13.232.193.224:27017/')
    mydb = myclient["stockdata"]
    mycol = mydb[candle]
    
    df = mask(mycol,start,end)
    result = relative(df)
    
    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},
        'body': json.dumps(result)
    }

