
# coding: utf-8

# In[ ]:


import requests
import json
from datetime import datetime 
import sys


# In[ ]:


if __name__ == "__main__":
    
    sym = sys.argv[1]
    candle = sys.argv[2]
    b = sys.argv[3]
    rule = sys.argv[4]
    entry = sys.argv[5]
    exit = sys.argv[6]
    
    url = "https://sczibqshr5.execute-api.ap-south-1.amazonaws.com/default/prototype?sym="+sym+"&candle="+candle+"&b="+b+"&rule="+rule+"&entry="+entry+"&exit="+exit
    myresponse = requests.get(url = url)
    myFile = open('log.txt', 'a')
    timestamp = datetime.now()
    date = datetime.strptime(str(timestamp).split(".")[0], '%Y-%m-%d %H:%M:%S')
    
    if(myresponse.ok): 
        myFile.write('success '+'@ '+ str(date)+"\n")
    else:
        myFile.write('failure '+'@ '+ str(date)+"\n")

