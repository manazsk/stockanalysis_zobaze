#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import json
import requests
from flask import Flask, redirect, render_template, request
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
from datetime import datetime
import pandas_datareader.data as web
import talib as ta
app = Flask(__name__)


# In[ ]:


def SMA(df,data,sma_list,colors):
    
    for i,j in zip(sma_list,colors):
        
        df['sma_'+i] = ta.SMA(df.close.values,timeperiod=int(i))
        
        sma = go.Scatter(
                y = df['sma_'+i],
                name = "SMA"+i,
                mode = 'line',
                line = dict(color = j),
                opacity = 0.7,
                visible = True,
            )
        data.append(sma)
        
    return data


# In[ ]:


def MA(df,data,ma_list,colors):
    
    for i,j in zip(ma_list,colors):
        
        df['ma_'+i] = ta.MA(df.close.values,timeperiod=int(i))
        
        ma = go.Scatter(
                
                y = df['ma_'+i],
                name = "MA"+i,
                mode = 'line',
                line = dict(color = j),
                opacity = 0.7,
                visible = True,
            )
        data.append(ma)
    #print(df)
        
    return data


# In[ ]:


def RSI(df,data,rsi_list,rsi_color):
    
    for i,j in zip(rsi_list,rsi_color):
        
        df['rsi'+i] = ta.RSI(df.close.values,timeperiod=int(i))
        
        rsi = go.Scatter(
        mode = 'line',
        y = df['rsi'+i],
        line = dict(color=j),
        name = 'RSI('+i+')',
        yaxis = 'y3'
        )
    
        data.append(rsi)
        
    return data


# In[ ]:


def STOCHRSI(df,data,stochrsi):
    
    n = int(stochrsi[0])
    fastk = int(stochrsi[1])
    fastd = int(stochrsi[2])
    
    df['fastk'],df['fastd'] = ta.STOCHRSI(df.close.values,timeperiod=n,fastk_period=fastk, fastd_period=fastd,fastd_matype=0)
    
    fastk = go.Scatter(
        mode = 'line',
        y = df['fastk'],
        line = dict(color='#0000FF'),
        name = '%K',
        yaxis = 'y3'
    )
    
    data.append(fastk)
        
    fastd = go.Scatter(
        mode = 'line',
        y = df['fastd'],
        line = dict(color='#B22222'),
        name = '%D',
        yaxis = 'y3'
    )
        
    data.append(fastd)
    
    return data


# In[ ]:


def MACD(df,data,macd):
    
    f = int(macd[0])
    sl = int(macd[1])
    sg = int(macd[2])
    
    df['macd'], df['macdSignal'], df['macdHist']  = ta.MACD(df.close.values,fastperiod=f,slowperiod=sl,signalperiod=sg)
        
    macd = go.Scatter(
        mode = 'line',
        y = df['macd'],
        line = dict(color='#00FFFF'),
        name = 'macd',
        yaxis = 'y2'
    )
    
    data.append(macd)
        
    macd2 = go.Scatter(
        mode = 'line',
        y = df['macdSignal'],
        line = dict(color='#FF0000'),
        name = 'macdSignal',
        yaxis = 'y2'
    )
        
    data.append(macd2)
    
    return data


# In[ ]:


def DONCH(df,data,period):  
    
    t = period[0]
    n = int(t)
    
    df['hband'] = df['close'].rolling(n).max()
    df['lband'] = df['close'].rolling(n).min()
    df['mband'] = df['hband'].add(df['lband'])
    df['mband']= df['mband'].div(2)
    
    upper = go.Scatter(
        mode = 'line',
        y  = df['hband'],
        name = 'hband',
        line = dict(color = '#62abc3')
    )
    
    data.append(upper)
    
    middle = go.Scatter(
        mode = 'line',
        y  = df['mband'],
        name = 'mband',
        line = dict(color = '#f92209'),
        fill = 'tonexty',
        fillcolor='rgba(45,164,210,0.5)'
    )
    
    data.append(middle)
    
    lower = go.Scatter(
        mode = 'line',
        y  = df['lband'],
        name = 'lband',
        line = dict(color = '#2da4d2'),
        fill='tonexty',
    )
    
    data.append(lower)
    
    return data


# In[ ]:


def AROON(df,data,aroon_list,colors):
    
    j = 0
    
    for i in aroon_list:
        
        df['aroondown'+i],df['aroonup'+i] = ta.AROON(df.high.values,df.low.values, timeperiod=int(i))
        
        aroon = go.Scatter(
        mode = 'line',
        y = df['aroondown'+i],
        line = dict(color=colors[j]),
        name = 'AROONDOWN('+i+')',
        yaxis = 'y2'
        )
    
        data.append(aroon)
        
        j = j+1
        
        aroon = go.Scatter(
        mode = 'line',
        y = df['aroonup'+i],
        line = dict(color=colors[j]),
        name = 'AROONUP('+i+')',
        yaxis = 'y2'
        )
        
        data.append(aroon)
        
    return data


# In[ ]:


def BBANDS(df,data,bbands):
    
    n = bbands[0]
    
    df['upperband'],df['middleband'], df['lowerband'] = ta.BBANDS(df.close.values, timeperiod=int(n), nbdevup=2, nbdevdn=2, matype=0)
    
    upper = go.Scatter(
        mode = 'line',
        y  = df['upperband'],
        name = 'upperband',
        line = dict(color = '#62abc3')
    )
    
    data.append(upper)
    
    middle = go.Scatter(
        mode = 'line',
        y  = df['middleband'],
        name = 'middleband',
        line = dict(color = '#f92209'),
        fill = 'tonexty',
        fillcolor='rgba(45,164,210,0.5)'
    )
    
    data.append(middle)
    
    lower = go.Scatter(
        mode = 'line',
        y  = df['lowerband'],
        name = 'lowerband',
        line = dict(color = '#2da4d2'),
        fill='tonexty',
    )
    
    data.append(lower)
    
    return data


# In[ ]:


def parse_data(data,bsize,rule):
    
    count = len(data['sub'])
    
    prev_close = int(data['main']['open'])
    
    prev_close = round(prev_close)
    
    iter_list = []
    time_list = []
    open_list = []
    close = []
    timestamp = []
    
    
    
    for i in range(1,count+1):
        i = str(i)
        iter_list.append(data['sub'][i]['p'])
        iter_list.append(data['sub'][i]['n'])
        
    k = -1

    #print(iter_list)
    
    for i in iter_list:
        q = int(i/bsize)
        rem = abs(i)%bsize
        if(rem > (bsize/2) and rule=='2'):
            c = abs(q)+1
        else:
            c = abs(q)
        for j in range(c):
            k = k + 1
            open_list.append(prev_close)
            if(i>0):
                close.append(open_list[k] + bsize)
            else:
                close.append(open_list[k] - bsize)
            prev_close = close[k]
            
    df = pd.DataFrame()
    
    low = []
    high = []
    
    for x,y in zip(open_list,close):
        if(x>y):
            high.append(x)
            low.append(y)
        else:
            low.append(x)
            high.append(y)
            
    
    df['open'] = open_list
    df['close']= close
    df['high'] = high
    df['low'] = low
    
    return df


# In[ ]:


@app.route('/')
def form():
    return render_template('front_end.html')


# In[ ]:


@app.route('/submit', methods= ["GET","POST"])
def parseform():
    
    result = request.form
    
    start = result['sdate']
    end = result['edate']
    candle = result['candle']
    bsize = float(result['bsize'])
    
    start = datetime.strptime(start, "%Y-%m-%d").strftime("%Y%m%d")
    end = datetime.strptime(end, "%Y-%m-%d").strftime("%Y%m%d")
    
    
    supert_length = list(filter(None,request.form.getlist('supert')))
    bbands = list(filter(None,request.form.getlist('bbands')))
    donch = list(filter(None,request.form.getlist('donch')))
    sma = list(filter(None,request.form.getlist('sma')))
    sma_color = list(filter(None,request.form.getlist('sma_color')))
    ma = list(filter(None,request.form.getlist('ma')))
    ma_color = list(filter(None,request.form.getlist('ma_color')))
    rsi = list(filter(None,request.form.getlist('rsi')))
    rsi_color = list(filter(None,request.form.getlist('rsi_color')))
    aroon = list(filter(None,request.form.getlist('aroon')))
    aroon_color = list(filter(None,request.form.getlist('aroon_color')))
    macd = list(filter(None,request.form.getlist('macd')))
    stochrsi = list(filter(None,request.form.getlist('stochrsi')))
    sma = list(filter(None,request.form.getlist('sma')))
    rule = result['rule']
    
    
    custom = str(0)
    URL = "http://13.232.193.224:1234/"+candle+"/"+start+"/"+end+"/"+custom
    r = requests.get(url = URL)
    
    data = r.json()
    df = parse_data(data,bsize,rule)
    
    trace = go.Candlestick(
                open=df['open'],
                high=df['high'],
                low=df['low'],
                close=df['close'])
    data = [trace]
    
    layout = go.Layout(
    title = 'SKD',
    font = dict(family='Droid Sans Mono'),
    margin = dict(
        l = 40,
        r = 40,
        b = 40,
        t = 40
    ),
    xaxis = dict(
        anchor = 'y3',
        rangeslider=dict(visible=False)
    ),
    xaxis2 = dict(
        anchor = 'y2',
        rangeslider=dict(visible=False)
    ),
    yaxis = dict(
        domain = [0.5, 1]
    ),
    yaxis2 = dict(
        domain = [0.25, 0.49],
    ),
    yaxis3 = dict(
        domain = [0, 0.24],
    )
)
    
    if(len(sma) != 0):
        data = SMA(df,data,sma,sma_color)
        
    if(len(ma) != 0):
        data = MA(df,data,ma,ma_color)
        
    
        
    if(len(rsi) != 0):
        data = RSI(df,data,rsi,rsi_color)
        
    if(len(aroon) != 0):
        data = AROON(df,data,aroon,aroon_color)
        
    if(len(bbands) != 0):
        data = BBANDS(df,data,bbands)
        
    if(len(donch) != 0):
        data = DONCH(df,data,donch)
        
    if(macd[0] != 'fastlength'):
        data = MACD(df,data,macd)   
    
    if(stochrsi[0] != 'length'):
        data = STOCHRSI(df,data,stochrsi)
    
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='simple_candlestick')
    
    return redirect('https://plot.ly/~manazsk/17/skd/')


# In[ ]:


if __name__ == "__main__":
    
    app.run(host = '0.0.0.0', port = 1235, debug = True)

