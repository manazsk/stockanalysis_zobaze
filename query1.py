#!/usr/bin/env python
# coding: utf-8

# In[4]:


from flask import Flask
from flask import jsonify
import time
import pymongo
import numpy as np
import json
from sshtunnel import SSHTunnelForwarder
from stockstats import StockDataFrame as Sdf
from pandas_datareader import data
import matplotlib.pyplot as plt
import pandas as pd
from datetime import datetime
from statistics import mean


# In[5]:


app = Flask(__name__)

def mask(mycol,start,end):
    start = datetime.strptime(str(start), '%Y%m%d').strftime('%Y-%m-%d')
    end  =  datetime.strptime(str(end), '%Y%m%d').strftime('%Y-%m-%d')
    
    start = pd.to_datetime(start).date()
    end = pd.to_datetime(end).date()
    
    cursor = mycol.find({},{"_id":0,"date":1,"close":1})
    df = pd.DataFrame(list(cursor))
    df['date'] = pd.to_datetime(df['date'])
    df = df[["date","close"]]
    
    df1 = pd.DataFrame()
    df1['date'] = df['date'].dt.date
    
    
    mask = (df1['date'] >= start) & (df1['date'] <= end)
    
    df = df.loc[mask]
    
    return df

def relative(df,custom):
    
    close_list = df['close'].tolist()
    date_list = df['date'].tolist()

    rel_dict = {}
    rel_dict['main'] = {}
    rel_dict['custom'] = {}
    rel_dict['sub'] = {}
    
    for i in range(1,len(close_list)):
        x = close_list[i] - close_list[i-1]
        x = round(x,2)
        rel_dict['sub'][i] = {}
        if(x>0):
            rel_dict['sub'][i]['p'] = x
            rel_dict['sub'][i]['n'] = 0
            rel_dict['sub'][i]['c'] = abs(x)
        elif(x<0):
            rel_dict['sub'][i]['n'] = x
            rel_dict['sub'][i]['p'] = 0
            rel_dict['sub'][i]['c'] = abs(x)
        else:
            rel_dict['sub'][i]['p'] = rel_dict['sub'][i]['n'] = rel_dict['sub'][i]['c'] = 0
            
        rel_dict['sub'][i]['timestamp'] = str(date_list[i])
        
    list_p = []
    list_n = []
    list_c = []
    
    for i in range(1,len(close_list)):
        list_p.append(rel_dict['sub'][i]['p'])
        list_n.append(rel_dict['sub'][i]['n'])
        list_c.append(rel_dict['sub'][i]['c'])
        
    rel_dict['main']['avg_p'] = round(mean(list_p),2)
    rel_dict['main']['avg_n'] = round(mean(list_n),2)
    rel_dict['main']['avg_c'] = round(mean(list_c),2)
    rel_dict['main']['open'] = close_list[0]
    rel_dict['main']['close'] = close_list[i]
    
    if(custom!=0):
        rel_dict['custom']['avg_p'] = round((sum(list_p)/custom),2)
        rel_dict['custom']['avg_n'] = round((sum(list_n)/custom),2)
        rel_dict['custom']['avg_c'] = round((sum(list_c)/custom),2)
            
    rel_json = json.dumps(rel_dict)
    rel_json = json.loads(rel_json)
    
    return rel_json
    
@app.route("/one/<start>/<end>/<int:custom>")
def one(start,end,custom):
    
    mycol = mydb["onemin"]
    
    df = mask(mycol,start,end)
    
    result = relative(df,custom)
    
    return jsonify(result)

@app.route("/five/<start>/<end>/<int:custom>")
def five(start,end,custom):
    
    mycol = mydb["fivemin"]
    
    df = mask(mycol,start,end)
    
    result = relative(df,custom)
    
    return jsonify(result)

@app.route("/fifteen/<start>/<end>/<int:custom>")
def fifteen(start,end,custom):
    
    mycol = mydb["fifteenmin"]
    
    df = mask(mycol,start,end)
    
    result = relative(df,custom)
    
    return jsonify(result)

@app.route("/thirty/<start>/<end>/<int:custom>")
def thirty(start,end,custom):
    
    mycol = mydb["thirtymin"]
    
    df = mask(mycol,start,end)
    
    result = relative(df,custom)
    
    return jsonify(result)

@app.route("/hour/<start>/<end>/<int:custom>")
def hour(start,end,custom):
    
    mycol = mydb["hour"]
    
    df = mask(mycol,start,end)
    
    result = relative(df,custom)
    
    return jsonify(result)


# In[6]:


if __name__ == "__main__":
    
    myclient = pymongo.MongoClient('localhost',27017)
    
    mydb = myclient["stockdata"]
    app.run(host = '0.0.0.0', port = 1234,debug = True)


# In[ ]:




